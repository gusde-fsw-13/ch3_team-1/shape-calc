const readline = require('readline');
const { stdin: input, stdout: output } = require('process');
const rl = readline.createInterface({ input, output });
// import the function
const square = require('./countSquare')
const circle = require('./countCircle')
const triangle = require('./countTriangle')
const trapezoid = require('./countTrapezoid')
const kite = require('./countKite')
const rectangular = require('./countRectangular')

function menu() {
    console.log('1. square')
    console.log('2. circle')
    console.log('3. triangle')
    console.log('4. trapezoid')
    console.log('5. kite')
    console.log('6. Rectangular')
}

console.clear();
menu();
rl.question('Choose shape : ', (option) => {
    if (option === '1') {
        rl.question('Input side : ', (side) => {
            console.log('Area of square : ' + square.countSquare(side));
            rl.close();
        })
    } else if (option === '2') {
        rl.question('Input radius : ', (radius) => {
            console.log('Area of Circle : ' + circle.countCircle(radius));
            rl.close();
        })
    } else if (option === '3') {
        rl.question('Input floor : ', (floor) => {
            rl.question('Input height : ', (height) => {
                console.log('Area of Triangle : ' + triangle.countTriangle(floor, height));
                rl.close();
            })
        })
        
    } else if (option === '4') {
        rl.question('Input floor : ', (floor) => {
            rl.question('Input ceil : ', (ceil) => {
                rl.question('Input height : ', (height) => {
                    console.log('Area of Trapezoid : ' + trapezoid.countTrapezoid(floor, ceil, height));
                    rl.close();
                })
            })
        })
    } else if (option === '5') {
        rl.question('Input first diagonal : ', (d1) => {
            rl.question('Input second diagonal : ', (d2) => {
                console.log('Area of Kite : ' + kite.countKite(d1, d2));
                rl.close();
            })
        })
        
    } 
    
    else if (option === '6') {
        rl.question('Input long : ', (p) => {
            rl.question('Input width : ', (l) => {
                console.log('Area of Rectangular : ' + rectangular.countRectangular(p, l));
                rl.close();
            })
        })
        
    } 

    else {
        console.log('Menu not found!')
        rl.close();
    }
});