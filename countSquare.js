function countSquare(a){
    return a * a;
}

module.exports = {
    countSquare
}