function countCircle(a) {
    return 3.14 * a * a;
}

module.exports = {
    countCircle
}