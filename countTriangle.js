function countTriangle(a, b) {
    return 0.5 * a * b;
}

module.exports = {
    countTriangle
}